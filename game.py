def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board):
    print_board(board)
    print(f"GAME OVER\n{current_player} has won")
    exit()

def is_row_winner(board, row_number):
    row_win = board[3 * row_number - 3] == board[3 * row_number - 2] and board[3 * row_number - 2] == board[3 * row_number - 1]
    return row_win

def is_column_winner(board, column_number):
    column_win = board[column_number - 1] == board[column_number + 2] and board[column_number + 2] == board[column_number + 5]
    return column_win

def is_diagonal_winner(board, diagonal_number):
    diagonal_win = board[2 * diagonal_number - 2] == board[4] and board[4] == board[-2 * diagonal_number + 10]
    return diagonal_win

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 1):
        game_over(board)
    elif is_row_winner(board, 2):
        game_over(board)
    elif is_row_winner(board, 3):
        game_over(board)
    elif is_column_winner(board, 1):
        game_over(board)
    elif is_column_winner(board, 2):
        game_over(board)
    elif is_column_winner(board, 3):
        game_over(board)
    elif is_diagonal_winner(board, 1):
        game_over(board)
    elif is_diagonal_winner(board, 2):
        game_over(board)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
